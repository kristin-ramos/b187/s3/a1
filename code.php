<?php

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	function __construct($firstName,$middleName, $lastName){
		$this->firstName=$firstName;
		$this->middleName=$middleName;
		$this->lastName=$lastName;
	}

	function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person ('Bebang', 'Ramos', 'Bagsit');

class Developer extends Person {
	public $job;

	function __construct($firstName,$middleName, $lastName, $job){
		$this->firstName=$firstName;
		$this->middleName=$middleName;
		$this->lastName=$lastName;
		$this->job=$job;
	}


	function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a $this->job .";
	}
}

$developer = new Developer ('Bebang', 'Ramos', 'Bagsit', 'developer');

class Engineer extends Developer {


	function printName(){
		return "Your are an $this->job named $this->firstName $this->middleName $this->lastName.";
	}
}

$engineer = new Engineer ('Bebang', 'Ramos', 'Bagsit', 'engineer');